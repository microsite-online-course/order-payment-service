<?php

use Illuminate\Support\Facades\Http;

function createPremiumAccess($data)
{
    $url = env("SERVICE_COURSE_URL") . "/api/v1/my-course/premium";

    try {
        $res = Http::timeout(10)->post($url, $data);
        $dataRes = $res->json();
        $dataRes["http_code"] = $dataRes->getStatusCode();
        return $dataRes;
    } catch (\Throwable $th) {
        return [
            "status" => "error",
            "http_code" => 500,
            "message" => "service my course unavailable"
        ];
    }
}
