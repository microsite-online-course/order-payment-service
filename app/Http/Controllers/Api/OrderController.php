<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


use App\Models\Order;

class OrderController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer",
                "user_uuid" => "uuid"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $userUuid = $req->user_uuid;
            $orders = Order::when($userUuid, function ($query) use ($userUuid) {
                return $query->where("user_uuid", $userUuid);
            });

            $total = $orders->count();
            $result = $orders->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    return [
                        "uuid" => $row->uuid,
                        "user_uuid" => $row->user_uuid,
                        "course_uuid" => $row->course_uuid,
                        "course_price" => $row->course_price,
                        "course_name" => $row->course_name,
                        "course_thumbnail" => $row->course_thumbnail,
                        "course_level" => $row->course_level,
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                        "snap_url" => $row->snap_url,
                        "metadata" => $row->metadata,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }

    public function store(Request $req)
    {
        try {
            DB::beginTransaction();
            $user = $req->user;
            $course = $req->course;

            $order = Order::create([
                "user_uuid" => $user["uuid"],
                "uuid" => Str::uuid(),
                "course_uuid" => $course["uuid"],
            ]);

            $midtransParams = [
                "transaction_details" => [
                    "order_id" => $order->uuid->toString(),
                    "gross_amount" => $course["price"],
                ],
                "customer_details" => [
                    "first_name" => $user["name"],
                    "email" => $user["email"],
                ]
            ];

            $midtransSnapUrl = $this->getMidtransSnapUrl($midtransParams);
            $order->snap_url = $midtransSnapUrl;
            $order->metadata = [
                "course_uuid" => $course["uuid"],
                "course_price" => $course["price"],
                "course_name" => $course["name"],
                "course_thumbnail" => $course['thumbnail'],
                "course_level" => $course["level"],
            ];
            $order->save();

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "course" => [
                        "uuid" => $course["uuid"],
                        "price" => $course["price"],
                        "thumbnail" => $course["thumbnail"],
                        "level" => $course["level"],
                    ],
                    "user" => [
                        "uuid" => $user["uuid"],
                        "name" => $user["name"],
                        "email" => $user["email"],
                    ],
                    "uuid" => $order->uuid,
                    "created_at" => $order->created_at,
                    "updated_at" => $order->updated_at,
                    "snap_url" => $order->snap_url,
                    "metadata" => $order->metadata,
                ],
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    private function getMidtransSnapUrl($params)
    {
        try {
            \Midtrans\Config::$serverKey = env("MIDTRANS_SERVER_KEY");
            \Midtrans\Config::$isProduction = (bool) env("MIDTRANS_PROD");
            \Midtrans\Config::$isSanitized = true;
            \Midtrans\Config::$is3ds =  (bool) env("MIDTRANS_IS3DS");

            return \Midtrans\Snap::createTransaction($params)->redirect_url;
        } catch (\Exception $e) {
            dd($e->getMessage(), $params);
        }
    }
}
