<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Models\Order;
use App\Models\PaymentLog;

class WebhookController extends Controller
{
    public function midtransHandler(Request $req)
    {
        try {
            DB::beginTransaction();
            $serverKey = env("MIDTRANS_SERVER_KEY");
            $data = $req->all();

            $signatureKey = $data["signature_key"];
            $orderUuid = $data["order_id"];
            $statusCode = $data["status_code"];
            $grossAmount = $data["gross_amount"];
            $transactionStatus = $data["transaction_status"];
            $paymentType = $data["payment_type"];
            $fraudStatus = $data["fraud_status"];

            $mySignatureKey = hash("sha512", $orderUuid . $statusCode . $grossAmount . $serverKey);

            if ($signatureKey != $mySignatureKey) {
                return response()->json([
                    "status" => "error",
                    "message" => "invalid signature"
                ], 400);
            }

            $order = Order::where("uuid", $orderUuid)->first();

            if (!$order) {
                return response()->json([
                    "status" => "error",
                    "message" => "order not found"
                ], 404);
            }

            if ($order->status == "success") {
                return response()->json([
                    "status" => "error",
                    "message" => "operation not permitted"
                ], 405);
            }

            if ($transactionStatus == "capture") {
                if ($fraudStatus == "challenge") {
                    $order->status = "challenge";
                } else if ($fraudStatus == "accept") {
                    $order->status = "success";
                }
            } else if ($transactionStatus == "settlement") {
                $order->status = "success";
            } else if (in_array($transactionStatus, ["cancel", "deny", "expired"])) {
                $order->status = "failure";
            } else if ($transactionStatus == "pending") {
                $order->status = "pending";
            }



            PaymentLog::create([
                "status" => $transactionStatus,
                "raw_response" => json_encode($data),
                "order_id" => $order->id,
                "uuid" => Str::uuid(),
                "payment_type" => $paymentType
            ]);

            $order->save();


            if ($order->status == "success") {
                createPremiumAccess([
                    "user_uuid" => $order->user_uuid,
                    "course_uuid" => $order->course_uuid,
                ]);
            }

            DB::commit();
            return response()->json([
                "status" => "success",
                "message" => "order payment completed"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e->getMessage()
            ]);
        }
    }
}
