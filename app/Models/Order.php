<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = ["uuid", "user_uuid", "course_uuid", "snap_url", "status", "metadata"];

    protected $cast = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
        'metadata' => 'array',
    ];

    public function orderLogs()
    {
        return $this->hasMany("App\Models\OrderLog")->orderBy("id", "asc");
    }
}
