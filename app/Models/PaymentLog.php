<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    protected $table = "payment_logs";
    protected $fillable = ["uuid", "order_id", "status", "payment_type", "raw_response"];

    protected $cast = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
        'raw_response' => 'array',
    ];

    public function order()
    {
        return $this->belongsTo("App\Models\Order");
    }
}
