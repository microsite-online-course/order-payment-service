<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('order')->group(function () {
        Route::get("/", "Api\OrderController@index");
        Route::post("/", "Api\OrderController@store");
    });

    Route::prefix('webhook')->group(function () {
        Route::post("/", "Api\WebhookController@midtransHandler");
    });
});
